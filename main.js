import Vue from 'vue'
import App from './App'
import uView from "uview-ui";
// 公共请求方法
import httpUtil from './common/httpUtil.js';
Vue.use(uView); 
Vue.config.productionTip = false
Vue.prototype.$request = httpUtil.http2;
App.mpType = 'app'

const app = new Vue({
    ...App
})
app.$mount()
