let islogin = false

// const baseUrl="http://localhost/" //正式地址
// const baseUrl = 'https://www.tongyeyou.net/api' //正式地址api.tongyeyou.com
const baseUrl = 'https://api.tongyeyou.com/api' //正式地址api.tongyeyou.com
// const baseUrl1 = 'https://www.tongyeyou.net' // 图片地址
const baseUrl1 = 'https://api.tongyeyou.com' // 图片地址

function commrequest(obj) {
	obj.header = obj.header || {}
	obj.showLoad = obj.showLoad || false
	// if (!obj.header['content-type']) {
	// 	obj.header['content-type'] = 'application/x-www-form-urlencoded'
	// }
	obj.header['Authorization'] = 'Bearer' + ' ' + uni.getStorageSync('guideToken')
	
	uni.request({
		url: baseUrl + obj.url,
		header: obj.header,
		data: obj.data,
		method: obj.method,
		success: res => {
			if (res.data.code != 200) {
				// 未登录
				if (res.data.code == 401) {
					// 为了展示出来弹框的内容采用定时器
					if (!islogin) {
						let time = null
						islogin = true
						time = setTimeout(() => {
							uni.removeStorageSync('guideToken')
							uni.navigateTo({
								url: '/pages/login/login'
							})
							time = null
							islogin = false
						}, 500)
					}
				}
			}
			obj.callBack(res)
		},
		fail: res => {
			obj.fail(res.data)
			uni.showToast({
				title: '系统维护中，请稍后',
				icon: 'none'
			})
		},
		complete: () => {
			if ( !obj.showLoad ) {
			uni.hideLoading()
			}
		}
	})
}

// ---------------------------------------    导出方法    ------------------------------------------
export default {
	commrequest,
	baseUrl,
	baseUrl1
}
