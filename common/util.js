import httpfun from './httpUtil.js'
// 获取某个dom节点信息
// type:select/selectAll
let get_element = function(dom, type = 'select') {
	return new Promise((res, rej) => {
		uni.createSelectorQuery()[type](dom).fields({
			size: true,
			scrollOffset: true
		}, (data) => {
			res(data);
		}).exec();
	});
}

// 可以获取元素的位置信息 top bottom right left....
let get_dom = function(dom, type = 'select') {
	return new Promise((res, rej) => {
		uni.createSelectorQuery()[type](dom).boundingClientRect(data => {
			res(data);
		}).exec();
	});
}

let get_dom_info = function(dom, type = 'select') {
	return new Promise((res, rej) => {
		uni.createSelectorQuery()[type](dom).boundingClientRect(data => {
			res(data);
		}).exec();
	});
}

// 获取 "navigationStyle": "custom" 中的状态栏高度和导航的高度(小程序端使用)

let status_nav_height = function() {
	let Height = uni.getSystemInfoSync().statusBarHeight
	// 获取胶囊按钮的信息
	let menuButtonInfo = uni.getMenuButtonBoundingClientRect()
	// console.log(Height,menuButtonInfo)
	// 获取并设置header距离顶部的高度
	// 获取顶部状态栏高度
	let status_height = menuButtonInfo.top
	let header_height = (menuButtonInfo.top - Height) * 2 + menuButtonInfo.height + Height;
	return {
		status_height,
		header_height
	}
}

// 判断变量的类型
let parseType = function(n) {
	// console.log(n)
	if (typeof n != 'object') {
		return typeof n
	}
	if (Array.prototype.isPrototypeOf(n)) {
		return 'array'
	} else {
		return 'object'
	}
}

// 校验手机号
let phoneFun = function(phones) {
	var myreg = /^[1][3,4,5,7,8,9][0-9]{9}$/;
	if (!myreg.test(phones)) {
		// console.log('手机号格式不正确')
		return false;
	} else {
		// console.log('手机号格式正确')
		return true;
	}
}

// 校验身份证
function IsCard(str) {
	var reg = /(^\d{15}$)|(^\d{18}$)|(^\d{17}(\d|X|x)$)/;
	return reg.test(str);
}

// 发送websocket通知
let sendMsg = function (obj) {
	httpfun.http({
		url: '/event',
		data: obj,
		callBack: function callBack (res) {
			console.log('发送成功', res)
		},
		fail: function fail (res) {
			console.log('发送失败', res)
		}
	})
}

export default {
	get_element,
	status_nav_height,
	get_dom,
	parseType,
	phoneFun,
	IsCard,
	sendMsg
}
