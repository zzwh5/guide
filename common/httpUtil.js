import commrequest from './commonrequest.js'

// 非promise封装
function http(obj) {
	var header = {}, showLoad = true
	var method = obj.method || 'POST'; 
	obj.header = obj.header || {};
	obj.data = obj.data || {};
	
	if(obj.showLoad){
		showLoad = false
	} else {
		showLoad = true
	}

	if (obj.header.accept) {
		header['Accept'] = obj.header.accept;
	}
	if (obj.header.content) {
		header['content-type'] = obj.header.content;
	} else {
		header['content-type'] = 'application/x-www-form-urlencoded';
	}
	
	commrequest.commrequest({
		url:obj.url,
		data:obj.data,
		method,
		showLoad,
		header,
		callBack:(res)=>{


			obj.callBack(res.data)
		},
		fail:(res)=>{
			obj.fail(res.data);
		}
	})
}

// promise封装
function http2(obj) {
	var header = {};
	var method = obj.method || 'POST';
	obj.header = obj.header || {};
	obj.data = obj.data || {};

	if (obj.header.accept) {
		header['Accept'] = obj.header.accept;
	}
	if (obj.header.content) {
		header['Content-Type'] = obj.header.content;
	} else {
		header['Content-Type'] = 'application/x-www-form-urlencoded';
	}
	return new Promise(function(resolve, reject) {
		commrequest.commrequest({
			url:obj.url,
			data:obj.data,
			method,
			header,
			callBack:(res)=>{
				resolve(res.data);
			},
			fail:(res)=>{
				reject('err')
			}
		})
	})
}
// 上传
function upload(obj) {
	var header = {}
	if (uni.getStorageSync('guideToken')) {
		header['api-token'] = uni.getStorageSync('guideToken')
	}
	uni.uploadFile({
		url: '/user/upload', //仅为示例，非真实的接口地址
		filePath: obj.path,
		name: 'file',
		header,
		formData: obj.formData,
		success: (res) => {
			if (typeof res.data == 'string') {
				res.data = JSON.parse(res.data)
			}
			// console.log(typeof  res.data)
			// console.log(res.data)
			// console.log(res.data.code)
			if (res.data.code == 401) {
				console.log('进来了啊')
				uni.navigateTo({
					url: '/pages/handle/login'
				})
				return
			}
			console.log(res)
			obj.success(res.data)
		},
		fail: (res) => {
			console.log(res)
			obj.fail(res.data)
		}
	});
}
// ----------------------------------------    导出方法    ------------------------------------------
export default {
	http: http,
	http2: http2,
	upload
}
